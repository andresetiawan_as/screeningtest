<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- home.php -->
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<!-- main content here -->
    <div>
        <h2 style="padding-left:16px;">Edit Movie <small>WebDB Cinemaks</small></h2>
    </div>
    <hr>
    <div style="padding:0px 16px 0px 16px;">
    	<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="col-sm-2">
              <label class="control-label pull-right">Title</label>
            </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="title" placeholder="Title" value="">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label class="control-label pull-right">Year</label>
            </div>
            <div class="col-sm-4">
              <input type="number" class="form-control" name="year" placeholder="Year" value="">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label class="control-label pull-right">Director</label>
            </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="director" placeholder="Director" value="">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label class="control-label pull-right">Current Poster</label>
            </div>
            <div class="col-sm-10">
              <img src="" width="200" height="300">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label class="control-label pull-right">New Poster</label>
            </div>
            <div class="col-sm-6">
              <input type="file" class="form-control" name="poster">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="submit" class="btn btn-default">Cancel</button>
            </div>
          </div>
        </form>
    </div>
</body>
</html>